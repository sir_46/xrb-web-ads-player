/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'www.xrbgalaxy.com',
        port: '',
        pathname: '/assets/**',
      },
      {
        protocol: 'https',
        hostname: 'adscmsapi.themonkgames.dev',
        port: '',
        pathname: '/image/**',
      },
    ],
  },
};

export default nextConfig;
