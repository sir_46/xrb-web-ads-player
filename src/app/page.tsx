export default function Home() {
  return (
    <main className='flex min-h-screen flex-col items-center justify-center'>
      <h1 className='text-xl font-bold'>XRB Ads player</h1>
    </main>
  );
}
