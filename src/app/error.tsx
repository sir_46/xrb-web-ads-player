'use client'; // Error components must be Client Components

import { Button } from '../components/ui/button';
import { useEffect } from 'react';

export default function Error({
  error,
  reset,
}: {
  error: Error & { digest?: string };
  reset: () => void;
}) {
  useEffect(() => {
    // Log the error to an error reporting service
    console.error(error);
  }, [error]);

  return (
    <div className='w-full h-screen flex items-center justify-center flex-col'>
      <h1 className='text-xl font-bold'>
        {error.message || 'Something went wrong!'}
      </h1>
      <Button
        data-testid='reset-button'
        variant='secondary'
        className='mt-4'
        onClick={() => typeof reset === 'function' && reset()}
      >
        Try again
      </Button>
    </div>
  );
}
