import BillboardPlayer from '@/components/BillboardPlayer';
import getCampainBillboard from '@/services/getCampainBillboard';

export const dynamic = 'force-dynamic';

type Props = {
  params: { id: string };
};

export const metadata = {
  title: 'Media',
};

const page = async ({ params }: Props) => {
  const id = params.id;
  const campain = await getCampainBillboard({ billboardId: id });

  if (campain.result?.media.length === 0) {
    throw new Error(campain.message || 'No media ads');
  }

  return <BillboardPlayer data={campain.result} />;
};

export default page;
