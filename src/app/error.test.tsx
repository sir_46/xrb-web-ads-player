import { cleanup, fireEvent, render } from '@testing-library/react';
import { describe, expect, it, afterEach, vi } from 'vitest';
import Error from './error';

const mockReset = vi.fn();

describe('Error page', () => {
  afterEach(() => {
    cleanup();
  });

  it('renders with default error  message and button', () => {
    const mockError: Error & { digest?: string } = {
      name: '',
      message: '',
      cause: {},
    };

    const { getByText } = render(<Error error={mockError} reset={() => {}} />);

    expect(getByText('Something went wrong!')).toBeDefined();
    expect(getByText('Try again')).toBeDefined();
  });

  it('calls reset function when "Try again" button is clicked', () => {
    const error: Error & { digest?: string } = {
      name: '',
      message: 'Test error message',
      cause: {},
    };

    const { getByText, getByTestId } = render(
      <Error error={error} reset={mockReset} />
    );

    expect(getByText('Test error message')).toBeDefined();
    fireEvent.click(getByText('Try again'));
    expect(mockReset).toHaveBeenCalled();
  });
});
