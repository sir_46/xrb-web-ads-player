import VRplayer from '@/components/vr-player';
import getVR from '@/services/getVR';

export const dynamic = 'force-dynamic';

type Props = {
  params: { id: string };
  searchParams?: { [key: string]: string | string[] | undefined };
};

export const metadata = {
  title: 'VR',
};

const page = async ({ params, searchParams }: Props) => {
  const id = params.id;
  const data = await getVR({ id: id });

  const avatarId = searchParams?.avatarId as string;

  if (!avatarId) {
    throw new Error('avatarId not found');
  }

  return <VRplayer data={data.result} avatarId={avatarId} />;
};

export default page;
