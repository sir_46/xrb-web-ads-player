import { describe, expect, test } from 'vitest';
import { render, screen } from '@testing-library/react';
import Page from './page';

describe('Page', () => {
  test('render heading', () => {
    render(<Page />);
    expect(
      screen.getByRole('heading', { level: 1, name: 'XRB Ads player' })
    ).toBeDefined();
  });
});
