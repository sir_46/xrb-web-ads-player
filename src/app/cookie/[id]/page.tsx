import CookiePlayer from '@/components/CookiePlayer';
import getCampainCookie from '@/services/getCampainCooike';
import React from 'react';

type Props = {
  params: { id: string };
};

export const metadata = {
  title: 'Cookie',
};

const page = async (props: Props) => {
  const id = props.params.id;
  const cookie = await getCampainCookie({ billboardId: id });

  if (cookie.result.media.length === 0) {
    throw new Error('No media ads');
  }

  return (
    <div>
      <CookiePlayer data={cookie.result} />
    </div>
  );
};

export default page;
