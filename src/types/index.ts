export interface IMedia {
  media_id: number;
  name: string;
  display_en: string;
  display_th: string;
  link: any;
  path: string;
  status: string;
  enable: boolean;
  created_at: string;
  updated_at: string;
}

export interface IVR {
  vr_id: number;
  name: string;
  display_en: string;
  display_th: string;
  desc_en: string;
  desc_th: string;
  link: any;
  path: string;
  countdown: number;
  viewer: number;
  enable: boolean;
  created_at: string;
  updated_at: string;
}

export interface IPostCounter {
  avatarId?: string;
  adsId: string;
}

export interface ICampainRespone {
  billboard_id: number;
  name: string;
  type: string;
  size: string;
  campains: ICampain[];
  media: IMedum[];
}

export interface ICampain {
  id: number;
  name: string;
  effective_start: string;
  effective_end: string;
}

export interface IMedum {
  id: number;
  name: string;
  path: string;
  link: any;
}
