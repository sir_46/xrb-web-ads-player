import { type ClassValue, clsx } from 'clsx';
import { twMerge } from 'tailwind-merge';

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs));
}

export const isImage = (name: string) => {
  if (!name) return false;
  const imageKeywords = ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'svg']; // Add more keywords as needed
  const lowercaseFilename = name.toLowerCase();

  return imageKeywords.some((keyword) => lowercaseFilename.includes(keyword));
};

export const isVideo = (name: string) => {
  if (!name) return false;
  const videoKeywords = ['mp4', 'avi', 'mov', 'mkv']; // Add more keywords as needed
  const lowercaseFilename = name.toLowerCase();

  return videoKeywords.some((keyword) => lowercaseFilename.includes(keyword));
};
