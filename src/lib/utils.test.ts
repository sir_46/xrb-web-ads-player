import { describe, expect, it } from 'vitest';
import { isImage, isVideo } from './utils';

describe('utils', () => {
  it('is image func', () => {
    const jpg = 'jpg';
    const jpeg = 'jpeg';
    const png = 'png';
    const svg = 'svg';

    // JPG file should be identified as an image
    const jpgfile = `image.${jpg}`;
    const isJPG = isImage(jpgfile);
    expect(isJPG).toBeTruthy();

    // JPEG file should be identified as an image
    const jpegfile = `image.${jpeg}`;
    const isJPEG = isImage(jpegfile);
    expect(isJPEG).toBeTruthy();

    // PNG file should be identified as an image
    const pngfile = `image.${png}`;
    const isPNG = isImage(pngfile);
    expect(isPNG).toBeTruthy();

    // SVG file should be identified as an image
    const svgfile = `image.${svg}`;
    const isSVG = isImage(svgfile);
    expect(isSVG).toBeTruthy();
  });

  it('is video func', () => {
    const mp4 = 'mp4';
    const avi = 'avi';
    const mov = 'mov';
    const mkv = 'mkv';

    // MP4 file should be identified as an video
    const mp4file = `video.${mp4}`;
    const isMP4 = isVideo(mp4file);
    expect(isMP4).toBeTruthy();

    // AVI file should be identified as an video
    const avifile = `video.${avi}`;
    const isAVI = isVideo(avifile);
    expect(isAVI).toBeTruthy();

    // MOV file should be identified as an video
    const movfile = `video.${mov}`;
    const isMOV = isVideo(movfile);
    expect(isMOV).toBeTruthy();

    // MKV file should be identified as an video
    const mkvfile = `video.${mkv}`;
    const isMKV = isVideo(mkvfile);
    expect(isMKV).toBeTruthy();
  });
});
