export default async function getVR({ id }: { id: string }) {
  const requestOptions: RequestInit = {
    method: 'GET',
    redirect: 'follow',
  };
  try {
    const res = await fetch(
      `${process.env.NEXT_PUBLIC_API_HOST}/vr/find/${id}`,
      requestOptions
    );

    if (!res.ok) {
      // This will activate the closest `error.js` Error Boundary
      throw new Error('Failed to fetch data');
    }

    return res.json();
  } catch (error) {
    return error;
  }
}
