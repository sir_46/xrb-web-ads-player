const getCampainCookie = async ({ billboardId }: { billboardId: string }) => {
  const requestOptions: RequestInit = {
    method: 'GET',
    redirect: 'follow',
  };

  try {
    const res = await fetch(
      `${process.env.NEXT_PUBLIC_API_HOST}/campains/cookie/${billboardId}`,
      requestOptions
    );

    return res.json();
  } catch (error) {
    return error;
  }
};

export default getCampainCookie;
