import { IPostCounter } from '@/types';

export default async function postCounter(payload: IPostCounter) {
  const requestOptions: RequestInit = {
    method: 'get',
    redirect: 'follow',
  };

  try {
    const res = await fetch(
      `${process.env.NEXT_PUBLIC_API_HOST}/vr/counter/${payload.adsId}/${payload.avatarId}`,
      requestOptions
    );

    if (!res.ok) {
      // This will activate the closest `error.js` Error Boundary
      throw new Error('Failed to fetch data');
    }

    return res.json();
  } catch (error) {
    return error;
  }
}
