export default async function getMedia({ id }: { id: string }) {
  const requestOptions: RequestInit = {
    method: 'GET',
    redirect: 'follow',
  };

  try {
    const res = await fetch(
      `${process.env.NEXT_PUBLIC_API_HOST}/media/find/${id}`,
      requestOptions
    );
    // The return value is *not* serialized
    // You can return Date, Map, Set, etc.

    if (!res.ok) {
      // This will activate the closest `error.js` Error Boundary
      throw new Error('Failed to fetch data');
    }

    return res.json();
  } catch (error) {
    return error;
  }
}
