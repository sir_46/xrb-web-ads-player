import React from 'react';

type Props = {
  timer: number;
};

const ControlTimer = (props: Props) => {
  return (
    <div className='absolute top-4 right-4 z-20'>
      <div
        className='flex items-center justify-center w-8 aspect-square bg-secondary rounded-full'
        data-testid='control-timer'
      >
        {props.timer}
      </div>
    </div>
  );
};

export default ControlTimer;
