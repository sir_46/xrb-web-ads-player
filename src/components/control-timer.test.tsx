import { render } from '@testing-library/react';
import { describe, expect, it } from 'vitest';
import ControlTimer from './ControlTimer';

describe('Control timer', () => {
  it('renders with correct timer', () => {
    const { getByText } = render(<ControlTimer timer={10} />);
    expect(getByText('10')).toBeDefined();
  });

  it('renders with updated timer', () => {
    const { getByText } = render(<ControlTimer timer={0} />);
    expect(getByText('0')).toBeDefined();
  });
});
