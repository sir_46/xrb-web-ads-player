import React from 'react';
import { Button } from './ui/button';
import { SpeakerLoudIcon, SpeakerOffIcon } from '@radix-ui/react-icons';

type Props = {
  isMouted: boolean;
  onClick: () => void;
};

const ControlSound = ({ isMouted, onClick }: Props) => {
  return (
    <div className='absolute top-4 left-4 z-20'>
      <Button
        size='icon'
        className='rounded-full'
        onClick={onClick}
        data-testid='mute-button'
      >
        {isMouted ? (
          <SpeakerOffIcon data-testid='speaker-off-icon' />
        ) : (
          <SpeakerLoudIcon data-testid='speaker-loud-icon' />
        )}
      </Button>
    </div>
  );
};

export default ControlSound;
