import React from 'react';

type Props = {
  current: number;
  total: number;
};

const ControlPage = (props: Props) => {
  return (
    <div className='absolute bottom-4 right-[calc(50%-16px)] z-10'>
      <div
        className='flex items-center justify-center w-10 bg-slate-900 bg-opacity-30 rounded px-2 py-1'
        data-testid='control-page'
      >
        {props.current + 1}/{props.total}
      </div>
    </div>
  );
};

export default ControlPage;
