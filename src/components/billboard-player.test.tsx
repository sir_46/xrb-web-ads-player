import { act, cleanup, fireEvent, render } from '@/lib/test-utils';

import { ICampainRespone } from '@/types';
import { describe, expect, it, beforeEach } from 'vitest';
import BillboardPlayer from './BillboardPlayer';

const mockData: ICampainRespone = {
  billboard_id: 0,
  name: 'billboard',
  type: 'type',
  size: '400',
  campains: [],
  media: [
    {
      id: 1,
      path: '/path/video.mp4',
      name: 'video',
      link: 'www.example.com',
    },
    {
      id: 2,
      path: '/path/image.jpg',
      name: 'image',
      link: 'www.example.com',
    },
  ],
};

beforeEach(() => {
  cleanup();
});

describe('Billboard Player', () => {
  it('renders without crashing', () => {
    const { getByText, getByTestId } = render(
      <BillboardPlayer data={mockData} />
    );

    expect(getByTestId('video')).toBeDefined();
    expect(getByText('1/2')).toBeDefined();
  });

  it('shows control page with current and total media count', () => {
    const { getByText } = render(<BillboardPlayer data={mockData} />);

    expect(getByText('1/2')).toBeDefined();
  });

  it('toggles mute/unmute on video click', () => {
    const { getByTestId } = render(<BillboardPlayer data={mockData} />);
    const videoElement = getByTestId('video') as HTMLVideoElement;

    // Initially muted
    expect(videoElement.muted).toBe(true);

    const mutedButton = getByTestId('mute-button');

    // Click on video to toggle mute/unmute
    fireEvent.click(mutedButton);
    expect(videoElement.muted).toBe(false);

    // Click again to toggle back to muted
    fireEvent.click(mutedButton);
    expect(videoElement.muted).toBe(true);
  });
});
