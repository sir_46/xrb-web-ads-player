'use client';
import React, { useEffect, useRef, useState } from 'react';
import { useMutation } from '@tanstack/react-query';
import { useParams } from 'next/navigation';
import postCounter from '@/services/postCounter';
import { IMedia, IPostCounter, IVR } from '@/types';
import ControlSound from './control-sound';
import { Button } from './ui/button';
import { PauseIcon, PlayIcon, ResetIcon } from '@radix-ui/react-icons';

type Props = {
  data: IMedia | IVR;
  loop?: boolean;
  avatarId?: string;
};

const Video = ({ data, loop, avatarId }: Props) => {
  const videoRef = useRef<HTMLVideoElement>(null);
  const [counter, setCounter] = useState<number>(0);
  const [muted, setMuted] = useState(true);
  const params = useParams();
  const [isPlaying, setIsPlaying] = useState(true);
  const [isButtonVisible, setIsButtonVisible] = useState(true);
  const timeoutRef = useRef<number | null>(null);

  // Mutations
  const mutation = useMutation({
    mutationFn: (payload: IPostCounter) => postCounter(payload),
    onSuccess: (data) => {
      // Invalidate and refetch
      console.log(data);
    },
  });

  useEffect(() => {
    const video = videoRef.current;
    if (video) {
      try {
        video.muted = true; // Ensure muted for autoplay
        video.play(); // Attempt playback

        handleMouseMove();
      } catch (error) {
        console.warn('Autoplay failed:', error);
      }

      videoRef.current.addEventListener('play', () => {
        setIsPlaying(true);
      });
      videoRef.current.addEventListener('pause', () => setIsPlaying(false));
    }

    return () => {
      video?.removeEventListener('play', () => setIsPlaying(true));
      video?.removeEventListener('pause', () => setIsPlaying(false));
    };
  }, [data]);

  const handleVideoEnded = () => {
    // Video ended, you can perform any necessary actions here
    setCounter(counter + 1);
    const video = videoRef.current;

    const payload: IPostCounter = {
      adsId: String(params.id),
      ...(avatarId !== undefined ? { avatarId } : {}),
    };

    mutation.mutate(payload);

    // loop
    loop && video?.play();
  };

  const handleMouseMove = () => {
    setIsButtonVisible(true);

    if (timeoutRef.current !== null) {
      clearTimeout(timeoutRef.current);
    }

    timeoutRef.current = window.setTimeout(() => {
      setIsButtonVisible(false);
    }, 2000); // Adjust the timeout duration as needed
  };

  useEffect(() => {
    window.addEventListener('mousemove', handleMouseMove);

    return () => {
      if (timeoutRef.current !== null) {
        clearTimeout(timeoutRef.current);
      }
      window.removeEventListener('mousemove', handleMouseMove);
    };
  }, []);

  if (!data) {
    return <p>Ads not found</p>;
  }

  return (
    <>
      <div className='relative z-0 group'>
        <ControlSound
          isMouted={muted}
          onClick={() => {
            const videoElement = videoRef.current;
            if (videoElement) {
              videoElement.muted = !muted; // Toggle mute/unmute
              setMuted(!muted); // Update the muted state
            }
          }}
        />
        <div className='absolute top-[calc(50%-36px/2)] left-[calc(50%-36px/2)] z-10'>
          {videoRef.current?.ended ? (
            <Button
              size='icon'
              className='rounded-full'
              onClick={() => {
                const video = videoRef.current;
                isPlaying ? video?.pause() : video?.play();
              }}
            >
              <ResetIcon />
            </Button>
          ) : isButtonVisible ? (
            <Button
              size='icon'
              className='rounded-full'
              onClick={() => {
                const video = videoRef.current;
                isPlaying ? video?.pause() : video?.play();
                setIsPlaying((prev) => !prev);
              }}
            >
              {isPlaying ? <PauseIcon /> : <PlayIcon />}
            </Button>
          ) : null}
        </div>
        <video
          data-testid='video'
          ref={videoRef}
          muted
          autoPlay
          className='w-full h-screen object-cover'
          onEnded={handleVideoEnded}
          preload='none'
        >
          <source
            src={`${process.env.NEXT_PUBLIC_MEDIA_HOST}${data.path}`}
            type='video/mp4'
          />
          Your browser does not support the HTML5 video tag.
        </video>
      </div>
    </>
  );
};

export default Video;
