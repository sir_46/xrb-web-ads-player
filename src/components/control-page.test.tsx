import { cleanup, render } from '@testing-library/react';
import { afterEach, describe, expect, it } from 'vitest';
import ControlPage from './ControlPage';

describe('Control Page', () => {
  it('renders with correct page numbers', () => {
    const props = {
      current: 2,
      total: 5,
    };
    const { getByText } = render(<ControlPage {...props} />);
    const pageNumberElement = getByText('3/5'); // current page + 1 / total pages
    expect(pageNumberElement).toBeDefined();
  });

  it('renders with updated page numbers', () => {
    const props = {
      current: 4,
      total: 10,
    };

    const { getByText } = render(<ControlPage {...props} />);
    const pageNumberElement = getByText('5/10'); // current page + 1 / total pages
    expect(pageNumberElement).toBeDefined();
  });
});
