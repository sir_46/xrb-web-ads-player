import { cleanup, fireEvent, render } from '@testing-library/react';
import { afterEach, describe, it, expect, vi } from 'vitest';
import ControlSound from './control-sound';

// Mocking onLick funtion
const onClickMock = vi.fn();

afterEach(() => {
  cleanup();
});

describe('Control Sound', () => {
  it('renders with loud speaker icon when mounted', () => {
    const { getByTestId } = render(
      <ControlSound isMouted={true} onClick={() => {}} />
    );
    const speakerOffIcon = getByTestId('speaker-off-icon');
    expect(speakerOffIcon).toBeDefined();
  });

  it('renders with loud speaker icon when unmounted', () => {
    const { getByTestId } = render(
      <ControlSound isMouted={false} onClick={() => {}} />
    );
    const speakerLoudIcon = getByTestId('speaker-loud-icon');
    expect(speakerLoudIcon).toBeDefined();
  });

  it('calls onClick handler when button is clicked', () => {
    const { getByRole } = render(
      <ControlSound isMouted={true} onClick={onClickMock} />
    );
    const button = getByRole('button');
    fireEvent.click(button);
    expect(onClickMock).toHaveBeenCalled();
  });
});
