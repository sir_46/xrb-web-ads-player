import { act, cleanup, render } from '@testing-library/react';
import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest';
import CookiePlayer from './CookiePlayer';
import { ICampainRespone } from '@/types';

const mockData: ICampainRespone = {
  billboard_id: 0,
  name: 'name',
  type: 'type',
  size: '3000',
  campains: [
    {
      id: 1,
      name: 'name',
      effective_start: '2024-05-01',
      effective_end: '2024-05-01',
    },
  ],
  media: [
    {
      id: 1,
      name: 'name',
      path: '/path',
      link: 'www.example1.com',
    },
    {
      id: 2,
      name: 'name',
      path: '/path',
      link: 'www.example2.com',
    },
  ],
};

// you can reset it in beforeEach hook manually
beforeEach(() => {
  vi.useFakeTimers();
  cleanup();
});

// afterEach(() => {
//   vi.useRealTimers();
// });

describe('Cookie player', () => {
  it('renders with inital state', () => {
    const { getByTestId } = render(<CookiePlayer data={mockData} />);

    const timer = getByTestId('control-timer');
    const page = getByTestId('control-page');
    expect(timer).toBeDefined();
    expect(page).toBeDefined();
  });

  it('updates current page and timer correctly', async () => {
    const { container, getAllByText } = render(
      <CookiePlayer data={mockData} />
    );

    act(() => {
      vi.advanceTimersByTime(1000); // Advance timer by 1 second
    });

    // After 1 second, timer should be 28 and current page should still be 1/2
    expect(getAllByText('28')).toBeDefined();
    expect(getAllByText('1/2')).toBeDefined();

    // act(() => {
    //   vi.advanceTimersByTime(30000); // Advance timer by 29 seconds
    // });

    // // After 30 second, timer should be 28 and current page should still be 2/2
    // expect(getAllByText('28')).toBeDefined();
    // expect(getAllByText('2/2')).toBeDefined();
  });
});
