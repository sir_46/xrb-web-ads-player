import { beforeEach, describe, it, vi, expect } from 'vitest';
import Video from './vr-player';
import { cleanup, fireEvent, render } from '@/lib/test-utils';

// Mocking useParams hook
vi.mock('next/navigation', () => ({
  useParams: vi.fn(() => ({ id: '123' })),
}));

const onClickMock = vi.fn();

// Mock the HTMLMediaElement.prototype.play method
HTMLMediaElement.prototype.play = vi.fn(() => Promise.resolve());

beforeEach(() => {
  cleanup();
});

describe('VR player', () => {
  const mockData = {
    media_id: 1,
    name: 'name',
    display_en: 'Display EN',
    display_th: 'DisplayTH',
    link: 'wwww.example.com',
    path: 'path/video.mp4',
    status: 'string',
    enable: true,
    created_at: '2024-5-1',
    updated_at: '2024-5-1',
  };

  it('renders video player correctly', () => {
    const { getByTestId, getByRole } = render(<Video data={mockData} />);

    // Assert that the video element is rendered
    const videoElement = getByTestId('video');
    expect(videoElement).toBeDefined();

    // Assert that the controls are rendered
    const controlButton = getByRole('button');
    expect(controlButton).toBeDefined();
  });

  it('updates counter and mutes/unmutes video on interaction', async () => {
    const { getByText, getByTestId, getByRole } = render(
      <Video data={mockData} />
    );

    // Assert initial counter value
    expect(getByText(/Counter: 0/i)).toBeDefined();

    const videoElm = getByTestId('video');

    // Simulate video end
    fireEvent(videoElm, new Event('ended'));

    // Assert updated counter value
    expect(getByText(/Counter: 1/i)).toBeDefined();

    // Toggle mute
    const muteButton = getByTestId('mute-button');
    fireEvent.click(muteButton);

    // Assert unmuted state
    expect((videoElm as HTMLVideoElement).muted).toBe(false);

    // Toggle mute again
    fireEvent.click(muteButton);

    // Assert muted state
    expect((videoElm as HTMLVideoElement).muted).toBe(true);
  });

  it('handles missing data gracefully', () => {
    const { getByText } = render(<Video data={null} />);

    // Assert that the component renders a message for missing data
    expect(getByText(/Ads not found/i)).toBeDefined();
  });
});
