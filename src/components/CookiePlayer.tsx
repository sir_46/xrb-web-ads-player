'use client';
import Image from 'next/image';
import { useEffect, useState } from 'react';
import ControlPage from './ControlPage';
import ControlTimer from './ControlTimer';
import { ICampainRespone } from '@/types';
type Props = {
  data: ICampainRespone;
};

const CookiePlayer = ({ data }: Props) => {
  const [timer, setTimer] = useState<number>(29);
  const [current, setCurrent] = useState<number>(0);

  useEffect(() => {
    // const intervalId = setInterval(() => {
    //   if (timer === 0) {
    //     setCurrent((prevIndex) => (prevIndex + 1) % data.media.length);
    //     setTimer(29); // Clear count
    //     return;
    //   }
    //   setTimer((prevCount) => prevCount - 1);
    // }, 1000);

    const interval = setInterval(() => {
      // Update timer every second
      setTimer((prevTimer) => prevTimer - 1);

      // Check if 30 seconds have passed (assuming you want to trigger something every 30s)
      if (timer % 30 === 0) {
        // Call your function here or trigger an action
        console.log('Trigger action!');
        setCurrent((prevIndex) => (prevIndex + 1) % data.media.length);
        setTimer(29); // Clear count
      }
    }, 1000); // Interval set to 1 second (1000 ms)

    return () => clearInterval(interval);
  }, [timer, data]);

  return (
    <div>
      <ControlTimer timer={timer} />
      <ControlPage current={current} total={data.media.length} />
      <Image
        fill
        className='object-contain'
        src={`${process.env.NEXT_PUBLIC_MEDIA_HOST || ''}${
          data.media[current]?.path
        }`}
        alt={'xrb'}
      />
    </div>
  );
};

export default CookiePlayer;
