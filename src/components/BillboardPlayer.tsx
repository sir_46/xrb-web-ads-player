'use client';

import { useCallback, useRef, useState } from 'react';
import ControlPage from './ControlPage';
import { isImage, isVideo } from '@/lib/utils';
import { ICampainRespone, IMedum } from '@/types';
import ControlSound from './control-sound';
import ImageTimer from './image-timer';

type Props = {
  data: ICampainRespone;
};

const BillboardPlayer = ({ data }: Props) => {
  const [current, setCurrent] = useState<number>(0);
  const [muted, setMuted] = useState(true);

  const videoRef = useRef<HTMLVideoElement>(null);

  const onEnded = useCallback(() => {
    const mediaLength = data.media.length - 1;

    // if media had 1
    if (current === mediaLength) {
      setCurrent(0);
      videoRef.current?.play(); // continue play
      return false;
    }

    // next media
    setCurrent((prev) => prev + 1);
  }, [current, data, videoRef]);

  const detectFileType = useCallback(
    (media: IMedum) => {
      const path = media.path;
      const name = media.name;
      if (isImage(path)) {
        return <ImageTimer path={path} name={name} onEnded={onEnded} />;
      }

      // Check if the filename contains video-related keywords
      if (isVideo(path)) {
        return (
          <video
            data-testid={name}
            ref={videoRef}
            key={path}
            autoPlay
            muted={muted}
            className='w-full h-screen object-cover'
            onEnded={onEnded}
          >
            <source
              src={`${process.env.NEXT_PUBLIC_MEDIA_HOST}${path}`}
              type='video/mp4'
            />
            Your browser does not support the HTML5 video tag.
          </video>
        );
      }
    },
    [muted, videoRef, onEnded]
  );

  return (
    <div>
      {isVideo(data.media[current]?.path) ? (
        <ControlSound
          isMouted={muted}
          onClick={() => {
            const videoElement = videoRef.current;
            if (videoElement) {
              videoElement.muted = !muted; // Toggle mute/unmute
              setMuted(!muted); // Update the muted state
            }
          }}
        />
      ) : null}
      <ControlPage current={current} total={data.media.length} />
      {detectFileType(data.media[current])}
    </div>
  );
};

export default BillboardPlayer;
