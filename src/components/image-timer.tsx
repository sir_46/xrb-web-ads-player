import Image from 'next/image';
import { useEffect, useState } from 'react';
import ControlTimer from './ControlTimer';

type Props = {
  path: string;
  name: string;
  duration?: number;
  onEnded: VoidFunction;
};

const ImageTimer = ({ path, name, duration = 29, onEnded }: Props) => {
  const [timer, setTimer] = useState<number>(duration);

  useEffect(() => {
    let interval: any;
    interval = setInterval(() => {
      // end of time
      if (timer === 0) {
        onEnded();
        setTimer(duration);
        return;
      }

      setTimer(timer - 1);
    }, 1000);

    return () => clearInterval(interval);
  }, [path, timer, duration, onEnded]);

  return (
    <div className='z-10'>
      <ControlTimer timer={timer + 1} />
      <Image
        fill
        className='object-cover'
        src={`${
          process.env.NEXT_PUBLIC_MEDIA_HOST || ''
        }${path}?${+new Date()}`}
        alt={name}
      />
    </div>
  );
};

export default ImageTimer;
